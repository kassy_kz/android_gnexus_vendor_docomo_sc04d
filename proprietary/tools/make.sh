#!/bin/sh

mkdir framework
adb pull /system/framework ./framework/
./apktool if ./framework/framework-res.apk

java -jar baksmali-1.3.2.jar -d ./framework -x ./framework/com.google.widevine.software.drm.odex
java -jar apktool.jar d ./framework/com.google.widevine.software.drm.jar "./apk.out"
mkdir ./apk.out/smali
cp -a ./out/* ./apk.out/smali/
java -jar apktool.jar b "./apk.out" "./framework/com.google.widevine.software.drm-unsigned.jar"
java -jar signapk.jar -w testkey.x509.pem testkey.pk8 ./framework/com.google.widevine.software.drm-unsigned.jar ./framework/com.google.widevine.software.drm-signed.jar
rm -rf ./apk.out
rm -rf ./out

cp -a ./framework/com.google.widevine.software.drm-signed.jar ./com.google.widevine.software.drm.jar

mkdir tmp
mkdir tmp/app

mkdir app
adb pull /system/app/CellBroadcastReceiver.apk ./app/
adb pull /system/app/CellBroadcastReceiver.odex ./app/

java -Xmx1024m -jar baksmali-1.3.2.jar -d ./framework -c core.jar:core-junit.jar:bouncycastle.jar:ext.jar:framework.jar:android.policy.jar:services.jar:apache-xml.jar:filterfw.jar:com.android.location.provider.jar -x ./app/CellBroadcastReceiver.odex
java -jar apktool.jar d ./app/CellBroadcastReceiver.apk "./apk.out"
mkdir ./apk.out/smali
cp -a ./out/* ./apk.out/smali/
java -jar apktool.jar b "./apk.out" "./tmp/app/CellBroadcastReceiver-unsigned.apk"
java -jar signapk.jar -w testkey.x509.pem testkey.pk8 ./tmp/app/CellBroadcastReceiver-unsigned.apk ./tmp/app/CellBroadcastReceiver.apk
rm ./tmp/app/CellBroadcastReceiver-unsigned.apk
cp -a ./tmp/app/CellBroadcastReceiver.apk ./CellBroadcastReceiver.apk
rm -rf ./apk.out
rm -rf ./out

rm -rf ~/apktool
rm -rf tmp
rm -rf framework
rm -rf app

